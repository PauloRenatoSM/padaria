package br.edu.cest.padariacest.excecao;

public class NaoEncontrado extends Exception {
	@Override
	public String getMessage() {
		return "Item nao encontrado";
	}
}
