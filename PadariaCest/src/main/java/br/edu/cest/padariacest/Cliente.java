package br.edu.cest.padariacest;

public class Cliente implements Item {
	
	private int codigo;
	private String nome;
	
	
	public Cliente(Integer cod, String n) {
		codigo = cod;
		nome = n;
	}

	@Override
	public Integer getCodigo() {
		return codigo;
	}

	@Override
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("---------------------\n");
		sb.append("Cod:" + codigo + "\n");
		sb.append("Nome:" + nome + "\n");
		return sb.toString();
	}


}
