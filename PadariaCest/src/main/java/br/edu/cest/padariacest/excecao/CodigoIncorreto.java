package br.edu.cest.padariacest.excecao;

public class CodigoIncorreto extends Exception {
	@Override
	public String getMessage() {
		return "Codigo invalido";
	}
}
