package br.edu.cest.padariacest;

public interface Item {
	public Integer getCodigo();
	public String getNome();
	public String toString();
}
