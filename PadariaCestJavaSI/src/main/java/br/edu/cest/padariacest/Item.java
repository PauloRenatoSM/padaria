package br.edu.cest.padariacest;

public interface Item {
	public Integer getCodigo();
	public String getNome();
	public String toString();
	public Integer getCpf();
	public String getEmail();
	public Integer getFone();
}
