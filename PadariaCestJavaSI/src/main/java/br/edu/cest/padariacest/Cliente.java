package br.edu.cest.padariacest;

public class Cliente implements Item {
	private int cpf;
	private int fone;
	private int codigo;
	private String nome;
	private String email;
	
	public Cliente(Integer cod,Integer cf,Integer fn, String n,String em ) {
		email = em;
		fone = fn;
		cpf = cf;
		codigo = cod;
		nome = n;
	}
	
	@Override
	public Integer getFone() {
		return fone;
	}
	
	@Override
	public String getEmail() {
		return email;
	}
	@Override
	public Integer getCpf() {
		return cpf;
	}
	
	@Override
	public Integer getCodigo() {
		return codigo;
	}

	@Override
	public String getNome() {
		return nome;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("---------------------\n");
		sb.append("Cod:" + codigo + "\n");
		sb.append("Nome:" + nome + "\n");
		sb.append("Email:" + email + "\n");
		sb.append("Cpf:" + cpf + "\n");
		sb.append("Telefone:" + fone + "\n");
		
		return sb.toString();
	}


}
